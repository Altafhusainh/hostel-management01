﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentsHostel.Models
{
    public class Hostel
    {
        public string studentName { get; set; }
        public int roomNo { get; set; }
        public int age { get; set; }
        public string joiningDate { get; set; }
    }
}
