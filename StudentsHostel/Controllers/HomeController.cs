﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentsHostel.Models;

namespace StudentsHostel.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            
                Hostel hostel = new Hostel();
                hostel.studentName = "Altaf";
                hostel.roomNo = 202;
                hostel.joiningDate = "12th DEC 2015";
                hostel.age = 27;

            Hostel hostel1 = new Hostel();
            hostel1.studentName = "Irfan";
            hostel1.roomNo = 203;
            hostel1.joiningDate = "13th DEC 2015";
            hostel1.age = 26;

            ViewBag.StudentName = hostel.studentName;
                ViewBag.RoomNumber = hostel.roomNo;
                ViewBag.JoiningDate = hostel.joiningDate;
                ViewBag.Age = hostel.age;

            ViewBag.StudentName1 = hostel1.studentName;
            ViewBag.RoomNumber1 = hostel1.roomNo;
            ViewBag.JoiningDate1 = hostel1.joiningDate;
            ViewBag.Age1 = hostel1.age;

            return View();
                
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
